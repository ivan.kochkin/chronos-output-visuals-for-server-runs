import numpy as np
import numpy_financial as npf
from dash import Dash, html, dcc
from dash.dependencies import Input, Output, State
import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots
import pandas as pd
import random
from argparse import ArgumentParser
from datetime import date
import os

app = Dash(__name__)

INPUT_BASE = './chronos_outputs/'
OUTPUT_PATHS = list(os.walk(INPUT_BASE))[0][2]

PENALTIES = 'Include penalties'


class Run:
    def __init__(self, name: str, data: pd.DataFrame):
        self.name = name
        self.data = data
        self.years = sorted(set(data.index.year))
        self.start_date = min(data.index.date)
        self.mask, self.mask_penalties = (
            f'_gross_margin{"_with_penalties" if penalties else ""}_currencyperkw'
            for penalties in (False, True)
        )
        self.markets = [
                column[:-len(self.mask)]
                for column in self.data.columns
                if (
                    self.mask in column
                    and 'wholesale' != column[:9]
                    and 'arbitrage' not in column
                    and 'total' not in column
                    and np.any(self.data[column])
            )
        ]


def give_me_plot(in_data):
    if in_data is None:
        return go.Figure()
    line_left, line_right, bar_right, data, index_range, title = in_data
    # Create figure with secondary y-axis
    fig = make_subplots(specs=[[{"secondary_y": True}]])

    if index_range:
        data = data.iloc[index_range[0]:index_range[1] + 1, :]
    # Add traces
    for col in bar_right:
        fig.add_trace(
            go.Bar(x=data.index, y=data[col], name=col),
            secondary_y=False,
        )

    for col in line_right:
        fig.add_trace(
            go.Scatter(x=data.index, y=data[col], name=col),
            secondary_y=False,
        )

    for col in line_left:
        fig.add_trace(
            go.Scatter(x=data.index, y=data[col], name=col, line={'dash': 'dash'}),
            secondary_y=True,
        )

    # Add figure title
    fig.update_layout(
        barmode='relative',
        title_text=title
    )

    # Set x-axis title
    fig.update_xaxes(title_text="Time")

    # Set y-axes titles
    fig.update_yaxes(title_text="Price", secondary_y=True)
    fig.update_yaxes(title_text="Dispatch / Capacity reserve", secondary_y=False)

    return fig


def give_me_data_for_plot(run, start_date, end_date):
    df = run.data
    df = df[df.index.date >= date(*[int(x) for x in start_date.split('-')])]
    df = df[df.index.date <= date(*[int(x) for x in end_date.split('-')])]
    if 'duration_h' not in df.columns:
        df['duration_h'] = 0

    for column in df.columns:
        if 'import' in column and 'price' not in column and min(list(df[column]) + [0]) < 0:
            df[column] = -df[column]
        if 'export' in column and 'price' not in column and max(list(df[column]) + [0]) > 0:
            df[column] = -df[column]
    line_left = [column for column in df.columns
                 if np.any([(market.split('_')[0] in column) for market in run.markets]) and 'price' in column]
    line_right = ['duration_h'] + [column for column in df.columns
                                   if np.any([(market.split('_')[0] in column) for market in run.markets])
                                   and 'capacity_h' in column
                                   and 'allocated' not in column]
    bar_right = [column for column in df.columns
                 if np.any([(market.split('_')[0] in column) for market in run.markets])
                 and np.any([(direction in column) for direction in ('import', 'export')])
                 and '_flh' in column
                 and 'allocated' not in column]
    for line_type in line_left, line_right, bar_right:
        yield [column for column in line_type if np.any(df[column])]
    for var_return in df, None, run.name:
        yield var_return


def multy_years_plots(run, years, capex, discount, fom, penalties_trigger):
    if capex is None:
        capex = 0
    if discount is None:
        discount = 0
    if fom is None:
        fom = 0

    run_name = run.name
    df = run.data
    markets = run.markets
    mask = run.mask_penalties if PENALTIES in penalties_trigger else run.mask
    columns_to_count = {
        market: [
            column
            for column in df.columns
            if np.all(
                [[market_part in column or (market_part == 'energy' and 'capacity' not in column)] for market_part in
                 market.split('_')])
               and (
                       'fulfilled_capacity_h' in column
                       or 'flh' in column
               )
               and 'allocated' not in column
               and np.any(df[column])
        ]
        for market in markets
    }

    cashflows = np.array(
        [sum(sum(df[df.index.year == year][f'{market}{mask}']) for market in markets) for year in years])
    cashflows[0] = cashflows[0] - capex
    cashflows = cashflows - fom
    if len([x for x in cashflows if x != 0]) > 1:
        irr = npf.irr(cashflows)
        if pd.isnull(irr):
            yield f'IRR cannot be calculated for run {run_name} because NPV is always positive with capex {capex} and fom {fom}'
        else:
            yield f'Calculated IRR for run {run_name}: {round(irr * 100, 2)}%'
    else:
        yield f'IRR calculations are unavailable for run {run_name}, because it contains only one year'

    for market in markets:
        df[f'{market}_export'] = sum(np.abs(df[column]) for column in columns_to_count[market] if 'export' in column)
        df[f'{market}_import'] = sum(-np.abs(df[column]) for column in columns_to_count[market] if 'import' in column)
    fig = go.Figure(data=[
                             go.Bar(
                                 name=f'{market}_export',
                                 x=list(years),
                                 y=[sum(df[df.index.year == year][f'{market}_export']) for year in years]
                             )
                             for market in markets
                         ] + [
                             go.Bar(name=f'{market}_import', x=list(years),
                                    y=[sum(df[df.index.year == year][f'{market}_import']) for year in years])
                             for market in markets
                         ])
    # Change the bar mode
    fig.update_layout(
        barmode='relative',
        title=f"{run_name} | Markets load",
        colorway=px.colors.qualitative.Plotly[:len(markets)]
    )
    yield fig

    fig = go.Figure(go.Waterfall(
        x=['Capex', 'FOM'] + markets + ['NPV'],
        measure=["absolute", "relative"] + ["relative" for _ in markets] + ["total"],
        y=[-capex, -sum(fom * (1 + discount) ** (min(years) - year) for year in years)] + [sum(
            sum(df[df.index.year == year][f'{market}{mask}']) * (1 + discount) ** (min(years) - year) for year in years)
            for market in markets] + [
              None],
    ))

    fig.update_layout(title=f"{run_name} | Profit and loss statement", waterfallgap=0.3)

    yield fig

    fig = go.Figure(data=[
        go.Bar(name=market, x=list(years), y=[sum(df[df.index.year == year][f'{market}{mask}']) for year in years])
        for market in markets
    ])
    fig.update_layout(title=f"{run_name} | Yearly profits and losses")
    yield fig


app.layout = html.Div(
    children=[
        dcc.Dropdown(
            id="Runs",
            options=OUTPUT_PATHS,
            placeholder="Select runs",
            multi=True,
        ),
        dcc.Checklist(
            id='penalties_trigger',
            options=[PENALTIES],
            value=[PENALTIES],
            inline=True,
        ),
        dcc.Tabs(
            [
                dcc.Tab(
                    label='Load by market',
                    id='tab_lt'
                ),
                dcc.Tab(
                    label='Revenues',
                    id='tab_rev',
                    children=[
                                 dcc.Input(
                                     id=f'input_{_}',
                                     type='number',
                                     placeholder=_,
                                 )
                                 for _ in ['Capex', 'Discount_rate', 'FOM']
                             ] + [
                                 html.Div(id='irr_value'),
                                 html.Div(id='revenues_plots')
                             ]
                ),
                dcc.Tab(
                    label='Revenues yearly',
                    id='tab_rev_yearly'
                ),
                dcc.Tab(
                    label='Dispatch',
                    id='tab_dispatch',
                    children=[
                        dcc.DatePickerRange(
                            id='dispatch_date_picker',
                            start_date=date(2022, 6, 22),
                            end_date=date(2022, 6, 23),
                            display_format='Y-M-D',
                            start_date_placeholder_text='Y-M-D'
                        ),
                        html.Button(
                            'Update graph',
                            id='dispatch_button'
                        ),
                        html.Div(
                            id='dispatch_plots'
                        )
                    ]
                )
            ]
        )
    ]
)


@app.callback(
    Output("irr_value", "children"),
    Output("tab_lt", "children"),
    Output("revenues_plots", "children"),
    Output("tab_rev_yearly", "children"),
    Output("dispatch_date_picker", "start_date"),
    Output("dispatch_date_picker", "end_date"),
    Input("Runs", "value"),
    *[Input(f"input_{_}", "value") for _ in ['Capex', 'Discount_rate', 'FOM']],
    Input("penalties_trigger", "value")
)
def fill_tabs(runs, capex, discount, fom, penalties_trigger):
    if runs is None:
        runs = []
    name_runs = {}
    years = None
    sample_date = None
    for run_csv in runs:
        run_data = pd.read_csv(f'{INPUT_BASE}{run_csv}').fillna(0)
        run_data.index = pd.DatetimeIndex(run_data['timestamp'], dayfirst=True)
        run_name = '.'.join(run_csv.split('.')[:-1])
        run = Run(run_name, run_data)
        name_runs[run_name] = run

        if years is None:
            years = set(run.years)
            sample_date = run.start_date
        else:
            years = years.union(set(run.years))
            sample_date = max(sample_date, run.start_date)
    if years is None:
        years = range(2022, 2071)
    if sample_date is None:
        sample_date = date(2023, 6, 15)
    years = sorted(years)
    content = [[] for _ in range(4)]
    for run in name_runs.values():
        for i, fig in enumerate(multy_years_plots(run, years, capex, discount, fom, penalties_trigger)):
            if i == 0:
                content[i].append(html.Div(fig))
            else:
                content[i].append(dcc.Graph(figure=fig))
    return content + [sample_date, sample_date]


@app.callback(
    Output("dispatch_plots", "children"),
    Input("dispatch_button", "n_clicks"),
    State("dispatch_date_picker", "start_date"),
    State("dispatch_date_picker", "end_date"),
    State("Runs", "value"),
)
def fill_tabs(n_clicks, start_date, end_date, runs):
    if runs is None:
        runs = []
    figs = []
    name_runs = {}
    for run_csv in runs:
        run_data = pd.read_csv(f'{INPUT_BASE}{run_csv}').fillna(0)
        run_data.index = pd.DatetimeIndex(run_data['timestamp'], dayfirst=True)
        run_name = '.'.join(run_csv.split('.')[:-1])
        run = Run(run_name, run_data)
        name_runs[run_name] = run
    for run in name_runs.values():
        figs.append(dcc.Graph(
            figure=give_me_plot(give_me_data_for_plot(run, start_date, end_date))
        ))
    return figs


if __name__ == '__main__':
    app.run_server(debug=True)
